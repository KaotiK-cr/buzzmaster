namespace BuzzMaster.Api.Models
{
    public class Human : Character
    {
        public string HomePlanet { get; set; }
    }
}
