namespace BuzzMaster.Api.Models
{
    public enum Episode
    {
        NEWHOPE = 4,
        EMPIRE = 5,
        JEDI = 6
    }
}
