namespace BuzzMaster.Api.Constants
{
    public static class CorsPolicyName
    {
        public const string AllowAny = nameof(AllowAny);
    }
}
