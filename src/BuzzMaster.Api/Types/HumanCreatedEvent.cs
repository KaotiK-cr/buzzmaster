namespace BuzzMaster.Api.Types
{
    using BuzzMaster.Api.Repositories;

    public class HumanCreatedEvent : HumanObject
    {
        public HumanCreatedEvent(IHumanRepository humanRepository) : base(humanRepository)
        {
        }
    }
}
